document.getElementById("newUsername").value = sessionStorage.getItem("username");
document.getElementById("cancelButton").addEventListener("click", function(){
    window.location.href = "../app.html";
});

/**
 * Makes HTTP PUT request to server for updating username and password
 */
function editUser (event) {
    let newUserName = document.getElementById("newUsername").value;
    let newPassword = document.getElementById("newPassword").value;
    event.preventDefault();
    let newInformation = {"username": newUserName, "password": newPassword};

    fetch('../api/user/'+sessionStorage.getItem("userId"), {
        method: "PUT",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(newInformation)
    })
        .then(response => response.json())
        .then(response => {
            if (response === true) {
                alert("Bruker oppdatert");
                sessionStorage.setItem("username", newUserName);
                window.location.href = "../app.html";
            } else {
                alert("Brukernavn eksisterer fra før, vennligst skriv inn et nytt brukernavn");
            }
        })
        .catch(error => console.error(error));
}
