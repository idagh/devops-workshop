import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = " 100 * 100 ";
        assertEquals("10000", calculatorResource.calculate(expression));

        expression = "200/100/2";
        assertEquals("1", calculatorResource.calculate(expression));


    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));

        expression = "300+99+100";
        assertEquals(499, calculatorResource.sum(expression));

        expression = "300+99+700";
        assertEquals(1099, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "300-10-5";
        assertEquals(285, calculatorResource.subtraction(expression));

        expression = "300-99-7";
        assertEquals(194, calculatorResource.subtraction(expression));
    }
    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "5*5";
        assertEquals(25, calculatorResource.multiplication(expression));

        expression = "8*8";
        assertEquals(64, calculatorResource.multiplication(expression));

        expression = "8*8*8";
        assertEquals(512, calculatorResource.multiplication(expression));

        expression = "4*8*8";
        assertEquals(256, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100/5";
        assertEquals(20, calculatorResource.division(expression));

        expression = "30/15";
        assertEquals(2, calculatorResource.division(expression));

        expression = "100/5/2";
        assertEquals(10, calculatorResource.division(expression));

        expression = "200/5/2";
        assertEquals(20, calculatorResource.division(expression));
    }
}
